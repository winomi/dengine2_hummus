const path = require('path');
const _ = require('lodash');
const Good = require('good');
const MrHorse = require('mrhorse');
const Routes = require('hapi-router');
const MySql = require('hapi-mysql2');
const Rollover = require('rollover');

const config = require('../config.js');

const plugins = new Array();

const logSqueezeArgs = [{
    log: '*',
    response: '*',
    request: '*',
    'request-internal': '*'
}];

plugins.push({
//const goodp = {
    plugin: Good,
    options: {
        reporters: {
            console: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: logSqueezeArgs
            }, {
                module: 'good-console',
                args: [{
                    format: 'HH:mm:ss DD.MM.YYYY'
                }]
            }, 'stdout'],
            file: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: logSqueezeArgs
            }, {
                module: 'good-squeeze',
                name: 'SafeJson'
            }, {
                module: 'rotating-file-stream',
                args: [
                    'log',
                    {
                        interval: '1d',
                        compress: 'gzip',
                        path: './logs'
                    }
                ]
            }]
        }
    }
});

const clientOpts = {
    settings: config.mySql,
    decorate: true
}
plugins.push({
    plugin: MySql,
    options: clientOpts
});

plugins.push({
//const mrhorsep = {
    plugin: MrHorse,
    options: {
        policyDirectory: path.join(__dirname, 'policies'),
        defaultApplyPoint: 'onPreHandler'
    }
});

plugins.push({
    plugin: Routes,
    options: {
        routes: 'routes/**/*.js'
    }
});

if (config.env == 2){  //test
    plugins.push({
        plugin: Rollover,
        options: {
            rollbar: config.rollbar,//"d62e7259b6104b4a8b3c7640029a4047",
            reportErrorResponses: true,
            reportServerLogs: true
        }
    });
}

// include and initialize the rollbar library with your access token
//var Rollbar = require("rollbar");
//var rollbar = new Rollbar("280cf6a462f04914a344c011475a2f56");
// record a generic message and send it to Rollbar
//rollbar.log("Hello world!");

/*
module.exports = server => new Promise((resolve, reject) => {
        server.register(plugins, (err) => {
        if(err) {
            reject(err);
        } else {
            resolve();
        }
    });
});
*/
/*
module.exports = async function(server) {
    var ind = 0;
    //for(ind; ind < plugins.length; ind++){
    //    await server.register(plugins[ind]);
    //}
    await server.register(goodp);
    await server.register(goodp);
};
*/
module.exports = plugins;