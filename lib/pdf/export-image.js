const fs = require('fs');
const path = require('path');
const pdf = require('pdf-poppler');
let request = require('request-promise-native');

//const env = require(global.appRoot + '/config.js').env;


//
// params:
// pid (processid - token)
// 
module.exports = async function(data){

    let fResponse = {
        statusCode: 200
    }

    //let pid = 'fa7f64c4-2d87-40e6-9bed-45b66c9ac452';
    let tempDirPath = global.appRoot + '/temp/' + data.pid;
    let tempPdfPath = tempDirPath + '/' + data.pid + '.pdf';

    try {
        if (require(global.appRoot + '/config.js').env == 1){  //dev
            console.log("env section");
            tempDirPath = global.appRoot + '/temp/fa7f64c4-2d87-40e6-9bed-45b66c9ac452';
            tempPdfPath = tempDirPath + '/fa7f64c4-2d87-40e6-9bed-45b66c9ac452.pdf';
            fResponse.images = [];
            fs.readdirSync(tempDirPath).forEach(file => {
                let f = fs.readFileSync(tempDirPath + "/" + file);
                if (path.extname(file) == '.png'){
                    fResponse.images.push(f.toString('base64'));
                    //fResponse.images.push(f);
                }
                if (path.extname(file) == '.pdf'){
                    fResponse.file = f;
                }
            })
    
        }else{
            console.log("test section");

            let body = await request.get({
                url: 'https://formfitt.com/Forms/API/PDF/5b68df902ffe5/5b69c3dce0266',
                encoding: null
            })
            fResponse.file = body;

            if (!fs.existsSync(tempDirPath)){
                await fs.mkdirSync(tempDirPath);
            }
            
            await fs.writeFileSync(tempPdfPath, body);
            
            let opts = {
                format: 'png',
                out_dir: tempDirPath,
                out_prefix: pid,
                page: null
            }
            pdf.convert(tempPdfPath, opts);
            
            fResponse.images = [];
            fs.readdirSync(tempDirPath).forEach(file => {
                if (path.extname(file) == '.png'){
                    let f = fs.readFileSync(tempDirPath + "/" + file);
                    //fResponse.files.push(Buffer.from(f.toString('base64')));
                    fResponse.images.push(f);
                }
            })

            await deleteFolderRecursive(tempDirPath);
        }

        return fResponse;
    }
    catch (err) {
        //return err;
        console.error(err);
        fResponse.statusCode = 505;
        return fResponse;
    }
}

var deleteFolderRecursive = async function(path) {
    if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function(file,index){
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(path);
    }
  };