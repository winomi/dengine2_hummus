//const bidi = require('icu-bidi');

module.exports  = function(inText,inDirection){

    //var p = bidi.Paragraph(inText,{paraLevel: inDirection == 'rtl' ? bidi.RTL:bidi.LTR});
    //return p.writeReordered(bidi.Reordered.KEEP_BASE_COMBINING);
    //return inText;
    if (isHeb(inText)){
        return reverseString(inText);
    }
    return inText;
}

function reverseString(str) {
    return str.split("").reverse().join("");
}

function isHeb(str){
    var position = str.search(/[\u0590-\u05FF]/);
    if(position >= 0){
        return true;
    }
    return false;
}