//const StringBuilder = require("string-builder");
//const moment = require('moment-timezone');
const hummus = require('hummus');
const bidi = require(global.appRoot + '/lib/pdf/bidi.js');
const request = require('request-promise-native');
const fs = require('fs');
const streams = require('memory-streams');

//var Stream = require('stream');

module.exports  = async function(data){

    let tempDirPath = global.appRoot + '/temp/dev';
    //let tempPdfPath = tempDirPath + '/dev.pdf';
    //const ws = fs.createWriteStream(tempDirPath + '/dev1.pdf');

    try{
        /*
        let ins = tempDirPath + '/dev5.pdf';
        let outs = tempDirPath + '/dev51.pdf';

        if(!fs.existsSync(ins)) 
            throw new Error('input does not exit');
        if(fs.existsSync(outs)) 
            fs.unlinkSync(outs)
        //let outs = fs.createReadStream(tempDirPath + '/dev4.pdf')
        //out.pipe(ws);
        await hummus.recrypt(ins, outs, {
                //password:'',
                userPassword: '',
                ownerPassword: '1111',
                userProtectionFlag: 4
                //log: tempDirPath + 'log.txt'                   
        });
*/
        /*
        var ws = new Stream();
        ws.writable = true;
        ws.bytes = 0;

        ws.write = function(buf) {
            ws.bytes += buf.length;
        }

        ws.end = function(buf) {
            if(arguments.length) ws.write(buf);
            ws.writable = false;

            console.log('bytes length: ' + ws.bytes);
        }
        */  
       
        let body = await request.get({
            //url: data.uri,
            url: 'https://formfitt.com/Forms/API/PDF/5b68df902ffe5/5b69c3dce0266',
            encoding: null
        });
        
        //var readr = new streams.ReadableStream(body);   
        //var writr = Buffer.alloc(15);//streams.WritableStream();     
        //var writr = streams.ReadableStream();     
        //let body_out = new streamBuffers.WritableStreamBuffer();
        //body.pipe(body_in);
        //var pBox = hummus.createReader(tempPdfPath).parsePage(0).getBleedBox();
        //var pdfWriter = hummus.createWriterToModify(tempPdfPath, {
        //    modifiedFilePath: tempDirPath + '/dev1.pdf'
        //});
        //var instream = new hummus.PDFRStreamForBuffer(body);

        /*
        let out = new hummus.PDFWStreamForFile(tempDirPath + '/dev.pdf');
        
        await hummus.recrypt(
            new hummus.PDFRStreamForBuffer(body), 
            out, 
            {
                //password:'',
                userPassword: '1',
                ownerPassword: '1111',
                userProtectionFlag: 4,
                log: tempDirPath + 'log.txt'                   
        });
*/
        let outStream = new streams.WritableStream();
        //let in = new hummus.pdf
        var pdfWriter = hummus.createWriterToModify(
            //instream,
            new hummus.PDFRStreamForBuffer(body)
            //out
            //tempDirPath + '/dev.pdf'//,
            ,new hummus.PDFStreamForResponse(outStream),
            /*
            {
                //password:'',
                userPassword: '1'//,
                //ownerPassword: '1111',
                //userProtectionFlag: 4                   
            }
            */
        );
        //var pBox = hummus.createReader(new hummus.PDFRStreamForBuffer(body)).parsePage(0).getBleedBox();
        /*
        var pBox = hummus.createReader(tempDirPath + '/dev.pdf',
        {
            //password:'',
            userPassword: '1'//,
            //ownerPassword: '1111',
            //userProtectionFlag: 4                   
        }).parsePage(0).getBleedBox();
        */
        //var pBox = hummus.createReader(instream).parsePage(0).getBleedBox();
        //var pdfWriter = hummus.createWriterToModify(body, new hummus.PDFStreamForResponse(writr));
        var page = pdfWriter.createPage(0, 0, 595.4, 842);
        //var page = pdfWriter.createPage(pBox[0], pBox[1], pBox[2], pBox[3]);

        let writer = pdfWriter.startPageContentContext(page);
        let arial = pdfWriter.getFontForFile(global.appRoot + '/resources/fonts/arial.ttf',0);
        writer
            .Tf(arial, 1)
            .k(0, 0, 0, 1);


        //1. name
        var text = bidi('מיכאל וינוגרד', 'rtl');
        writer
            .BT()
            .Tm(12, 0, 0, 12, 78.4252, 682.8997)
            .Tj(text)
            .ET();


        //2. contacted by:
        text = 'Contacted by: ';
        text += bidi('ktree.co.il');
        writer
            .BT()
            .Tm(12, 0, 0, 12, 78.4252, 652.8997)
            .Tj(text)
            .ET();

        //3. Created:
        text = 'Created:';
        pdfWriter.startPageContentContext(page)
            .BT()
            .Tm(12,0,0,12,78.4252,622.8997)
            .Tj(text)
            .ET();

        /*
        pdfWriter.startPageContentContext(page)
            .BT()
            .k(0,0,0,1)
            .Tf(pdfWriter.getFontForFile(global.appRoot + '/resources/fonts/arial.ttf',0),0.5)
            .Tm(30,0,0,30,78.4252,682.8997)
            .Tj(text)
            .ET();

        //2. contacted by:
        text = 'Contacted by:';
        pdfWriter.startPageContentContext(page)
            .BT()
            .k(0,0,0,1)
            .Tf(pdfWriter.getFontForFile(global.appRoot + '/resources/fonts/arial.ttf',0),0.5)
            .Tm(30,0,0,30,78.4252,652.8997)
            .Tj(text)
            .ET();

        //3. Created:
        text = 'Created:';
        pdfWriter.startPageContentContext(page)
            .BT()
            .k(0,0,0,1)
            .Tf(pdfWriter.getFontForFile(global.appRoot + '/resources/fonts/arial.ttf',0),0.5)
            .Tm(30,0,0,30,78.4252,622.8997)
            .Tj(text)
            .ET();
*/    
        pdfWriter.writePage(page);
        pdfWriter.end();
/*
        let ins = tempDirPath + '/dev5.pdf';
        let outs = tempDirPath + '/dev51.pdf';

        if(!fs.existsSync(ins)) {
            throw new Error('input does not exit');
        }
        if(fs.existsSync(ins)) {
            fs.unlinkSync(ins);
        }
        let insO = fs.createReadStream(ins);
        if(fs.existsSync(outs)) {
            fs.unlinkSync(outs);
        }
*/        
        //let outs = fs.createReadStream(tempDirPath + '/dev4.pdf')
        //out.pipe(ws);
        //await hummus.recrypt(ins, outs, {
        
        hummus.recrypt(new hummus.PDFRStreamForBuffer(outStream.toBuffer())
        //, tempDirPath + '/dev1.pdf',
            ,new hummus.PDFWStreamForFile(tempDirPath + '/dev.pdf'), 
            {
                //password:'',
                userPassword: '',
                ownerPassword: '1111',
                userProtectionFlag: 4
                //log: tempDirPath + 'log.txt'                   
        });
        
    }catch(err){
        console.error(err);
    }
}