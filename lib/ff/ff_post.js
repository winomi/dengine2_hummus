const wreck = require('wreck');
 
const method = 'POST'; // GET, POST, PUT, DELETE
//const uri = '/';
//const readableStream = Wreck.toReadableStream('foo=bar');
/* 
const wreck = Wreck.defaults({
    headers: { 'x-foo-bar': 123 },
    agents: {
        https: new Https.Agent({ maxSockets: 100 }),
        http: new Http.Agent({ maxSockets: 1000 }),
        httpsAllowUnauthorized: new Https.Agent({ maxSockets: 100, rejectUnauthorized: false })
    }
});
*/ 
// cascading example -- does not alter `wreck`
// inherits `headers` and `agents` specified above
/*
const wreckWithTimeout = wreck.defaults({
    timeout: 5
});
*/
// all attributes are optional
const options = {
    baseUrl: 'https://formfitt.com/Forms/API/',
    //payload: readableStream || 'foo=bar' || new Buffer('foo=bar'),
    //headers: { /* http headers */ },
    //redirects: 3,
    //beforeRedirect: (redirectMethod, statusCode, location, resHeaders, redirectOptions, next) => next(),
    //redirected: function (statusCode, location, req) {},
    //timeout: 1000,    // 1 second, default: unlimited
    //maxBytes: 1048576, // 1 MB, default: unlimited
    //rejectUnauthorized: true || false,
    //downstreamRes: null,
    //agent: null,         // Node Core http.Agent
    //secureProtocol: 'SSLv3_method', // The SSL method to use
    //ciphers: 'DES-CBC3-SHA' // The TLS ciphers to support
};
 
module.exports = async function(action, payload){
//const example = async function () {
    options.payload = payload;
    const promise = wreck.request(method, action, options);
    try {
        const prom = await promise;
        const body = await wreck.read(prom);
        //console.log(body.toString());
        let res = body.toString().replace('<xml version="1.0" encoding="UTF-8">', 
                                            '<?xml version="1.0" encoding="UTF-8"?>');
        
        //resolve(prom);
        return res;
    }
    catch (err) {
        // Handle errors
        //reject(err);
        return err;
    }
    //return promise
//};
}