const StringBuilder = require("string-builder");
const moment = require('moment-timezone');

module.exports  = function(data){

    let formParam = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    formParam += "<form>";
    formParam += "<info>";	
    formParam += "<key>74ad85d8e367a7b0d522fce7e2746d9b</key>";
    //formParam += "<formid>empty</formid>";
    formParam += "<formid>" + data.formid + "</formid>";
    formParam += "<externalid>" + data.id + "</externalid>";
    formParam += "<id>" + data.id + "</id>";
    formParam += "</info>";  

    //data fields
    formParam += "<fields>";
    formParam += "<field name=\"employeeId\">" + data.employeeid + "</field>";
    formParam += "<field name=\"firstName\">" + data.firstName + "</field>";
    formParam += "<field name=\"familyName\">" + data.familyName + "</field>";
    formParam += "<field name=\"startDate\">" + data.startDate + "</field>";
    formParam += "<field name=\"fundName\">" + data.fundName + "</field>";
    formParam += "<field name=\"depositPercent\">" + data.depositPercent + "</field>";
    formParam += "<field name=\"accumulatePercent\">" + data.accumulatePercent + "</field>";
    formParam += "<field name=\"today\">" + data.today + "</field>";
    formParam += "<field name=\"sign_1\">" + data.sign_1 + "</field>";

    formParam += "<field name=\"birthDate\">" + data.birthDate + "</field>";

    //gender
    if (data.gender == 1){
        formParam += "<field name=\"gender_1\">" + 1 + "</field>";
        formParam += "<field name=\"gender_2\">" + 0 + "</field>";
    } else{
        formParam += "<field name=\"gender_1\">" + 0 + "</field>";
        formParam += "<field name=\"gender_2\">" + 1 + "</field>";
    }

    //family status
    if (data.familyStatus == 1){
        formParam += "<field name=\"familyStatus_1\">" + 1 + "</field>";
        formParam += "<field name=\"familyStatus_2\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_3\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_4\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_5\">" + 0 + "</field>";
    }
    if (data.familyStatus == 2){
        formParam += "<field name=\"familyStatus_1\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_2\">" + 1 + "</field>";
        formParam += "<field name=\"familyStatus_3\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_4\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_5\">" + 0 + "</field>";
    }
    if (data.familyStatus == 3){
        formParam += "<field name=\"familyStatus_1\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_2\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_3\">" + 1 + "</field>";
        formParam += "<field name=\"familyStatus_4\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_5\">" + 0 + "</field>";
    }
    if (data.familyStatus == 4){
        formParam += "<field name=\"familyStatus_1\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_2\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_3\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_4\">" + 1 + "</field>";
        formParam += "<field name=\"familyStatus_5\">" + 0 + "</field>";
    }
    if (data.familyStatus == 5){
        formParam += "<field name=\"familyStatus_1\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_2\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_3\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_4\">" + 0 + "</field>";
        formParam += "<field name=\"familyStatus_5\">" + 1 + "</field>";
    }
    formParam += "<field name=\"city\">" + data.city + "</field>";
    formParam += "<field name=\"street\">" + data.street + "</field>";
    formParam += "<field name=\"house\">" + data.house + "</field>";
    formParam += "<field name=\"email\">" + data.email + "</field>";

    let cell = data.cell.replace("972", "0");
    formParam += "<field name=\"cell\">" + cell + "</field>";
    //formParam += "<field name=\"cell\">" + data.cell + "</field>";

    formParam += "<field name=\"employmentType_1\">" + 1 + "</field>";
    
    //insurance path - default path
    formParam += "<field name=\"insurancePath_0\">" + 1 + "</field>";
    if (data.gender == 1){
        formParam += "<field name=\"insurancePath_0_1\">" + 1 + "</field>";
    }else{
        formParam += "<field name=\"insurancePath_0_2\">" + 1 + "</field>";
    }
    //investment path - default path
    formParam += "<field name=\"insurancePath_1\">" + 1 + "</field>";

    formParam += "<field name=\"employerName\">" + data.employerName + "</field>";

    var employerAddr = "";
	if (!data.employerCity || data.employerCity == "")
        employerAddr = "";
	else if (!data.employerStreet || data.employerStreet == "")
        employerAddr = data.employerCity;
	else if (!data.employerHouse || data.employerHouse == "")
        employerAddr = data.employerStreet + ", " + data.employerCity;
	else 
        employerAddr = data.employerStreet + " " + data.employerHouse + ", " + data.employerCity;

    formParam += "<field name=\"employerAddress\">" + employerAddr + "</field>";
    formParam += "<field name=\"employerId\">" + data.employerid + "</field>";
    formParam += "<field name=\"employerPhone\">" + data.employerPhone + "</field>";
    formParam += "<field name=\"accumulatePercent\">" + data.accumulatePercent + "</field>";
    formParam += "<field name=\"depositPercent\">" + data.depositPercent + "</field>";

    formParam += "<field name=\"vatika_1\">" + data.vatika + "</field>";
    formParam += "<field name=\"menahalim_1\">" + data.menahalim + "</field>";

    //connection method - default: via mail
    formParam += "<field name=\"connectionMethod_1\">" + 1 + "</field>";

    formParam += "</fields>";
    formParam += "</form>";

    return formParam;
}