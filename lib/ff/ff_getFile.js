const fs = require('fs');
const path = require('path');
const pdf = require('pdf-poppler');
const request = require('request-promise-native');
const uuidv4 = require('uuid/v4');
const dbox = require(global.appRoot + '/lib/storage/dropbox_upload.js');
//const audit = require(global.appRoot + '/lib/pdf/add_audit.js');

//
// params:
// pid (processid)
// images (true/false)
// uri (url)
module.exports = async function(type, data){  

    let fResponse = {
        statusCode: 200
    }

    let tempDirPath = global.appRoot + '/temp/' + uuidv4();
    let tempPdfPath = tempDirPath + '/' + uuidv4() + '.pdf';

    try {
    
        if (require(global.appRoot + '/config.js').env == 1 && data.images == 1){  //dev
            console.log("dev section");
            //no images for dev
            data.images = 0
        }
            
        let body = await request.get({
            url: data.uri,
            encoding: null
        })

        //if this is a final file need to add audit data
        if (type == 'final'){

        }

        ////
        //console.log("got pdf from formfitt");
        
        //save pdf to dropbox
        let pdfname = data.userid + '_pensia_' + type;
        let pdfpath = data.clientid + "/" + data.employerid + "/" + data.userid;
        let dbxFile = await dbox(body, pdfname, pdfpath);

        if (data.pdf == 1)
            fResponse.file = body.toString('base64');

        fResponse.images = [];


        ////
        //console.log("==============================");
        //console.log("data.images == " + data.images);
        //console.log("tempDirPath == " + tempDirPath);

        //create images from the pdf
        if (data.images == 1){

            if (!fs.existsSync(tempDirPath)){
                await fs.mkdirSync(tempDirPath);
            } else{
                ////
                //console.log("tempDirPath exists... deleting conent");
                await deleteFolderRecursive(tempDirPath);
            }
            
            await fs.writeFileSync(tempPdfPath, body);
            
            let opts = {
                format: 'png',
                out_dir: tempDirPath,
                out_prefix: data.pid,
                page: null
            }
            await pdf.convert(tempPdfPath, opts);

            ////
            console.log("converted pdf");
            
            fs.readdirSync(tempDirPath).forEach(file => {
                if (path.extname(file) == '.png'){
                    let f = fs.readFileSync(tempDirPath + "/" + file);
                    fResponse.images.push(f.toString('base64'));
                    //fResponse.images.push(f);
                    
                    ///
                    //console.log("pushed " + file);
                }
            })

            ////
            console.log("got images");

            await deleteFolderRecursive(tempDirPath);

            ////
            console.log("deleted temp dir");
        }
        //}

        return fResponse;
    }
    catch (err) {
        //return err;
        console.error(err);
        fResponse.statusCode = 505;
        fResponse.error = err;
        return fResponse;
    }
}

var deleteFolderRecursive = async function(path) {
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
      });
      fs.rmdirSync(path);
    }
};