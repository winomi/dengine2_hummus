const Joi = require('joi');
const Boom = require('boom');
const StringBuilder = require("string-builder");
const moment = require('moment-timezone');
var parseXML = require('xml2js').parseString;

const mySQLQuery = require(global.appRoot + '/lib/db/dbquery.js');
const ffPost = require(global.appRoot + '/lib/ff/ff_post.js');
const ffPayload = require(global.appRoot + '/lib/ff/ff_payload.js');  //payload fot final
const ffPayload_p = require(global.appRoot + '/lib/ff/ff_payload_preview.js'); //payload for preview
const ffGetFile = require(global.appRoot + '/lib/ff/ff_getFile.js');
//const selectUserString = require(global.appRoot + '/lib/db/strings/authenticateUserString.js');
//const env = require(global.appRoot + '/config.js').env;


let selectPolicyDataString = "";
selectPolicyDataString += "SELECT";
selectPolicyDataString += " user.userid";
selectPolicyDataString += ", user.clientid";
selectPolicyDataString += ", user.employerid";
selectPolicyDataString += ", user.first_name ";
selectPolicyDataString += ", user.family_name ";
selectPolicyDataString += ", user.birth_date ";
selectPolicyDataString += ", user.gender ";
selectPolicyDataString += ", user.family_status ";
selectPolicyDataString += ", user.start_date ";
selectPolicyDataString += ", user.city ";
selectPolicyDataString += ", user.street ";
selectPolicyDataString += ", user.house ";
selectPolicyDataString += ", user.cell ";
selectPolicyDataString += ", user.email ";
selectPolicyDataString += ", user.process_data ";
selectPolicyDataString += ", employer.name as employerName";
selectPolicyDataString += ", employer.city as employerCity";
selectPolicyDataString += ", employer.street as employerStreet";
selectPolicyDataString += ", employer.house as employerHouse";
selectPolicyDataString += ", employer.contact_phone as employerPhone";
selectPolicyDataString += ", keren.name as keren_name";
selectPolicyDataString += ", keren.dmei_ninul_hafkada";
selectPolicyDataString += ", keren.dmei_nihul_tzvira";
selectPolicyDataString += ", keren.formid";
selectPolicyDataString += ", keren.formid_audit";
selectPolicyDataString += ", audit.name";
selectPolicyDataString += ", audit.contacted_by";
selectPolicyDataString += ", audit.created_by";
selectPolicyDataString += ", audit.documentCreated_at";
selectPolicyDataString += ", audit.documentCreated_remoteAddress";
selectPolicyDataString += ", audit.documentViewed_at";
selectPolicyDataString += ", audit.documentViewed_remoteAddress";
selectPolicyDataString += ", audit.documentSigned_at";
selectPolicyDataString += ", audit.doumentSigned_remoteAddress";
selectPolicyDataString += ", audit.documentLocked_at";
selectPolicyDataString += ", audit.documentLocked_remoteAddress";
//selectPolicyDataString += ", ff.fillid";
selectPolicyDataString += " FROM processes user ";
//selectPolicyDataString += "LEFT JOIN formfitt ff ON ff.processid = user.processid ";
selectPolicyDataString += "LEFT JOIN process_audit audit ON audit.processid = user.processid ";
selectPolicyDataString += "LEFT JOIN kranot keren ON keren.kerenid = user.kerenid ";
selectPolicyDataString += "LEFT JOIN employers employer ON employer.id = user.employerid ";
selectPolicyDataString += "WHERE ";
selectPolicyDataString += "user.processid = '{0}'"

let selectFormfitt = "";
selectFormfitt += "SELECT";
selectFormfitt += " ffid";
selectFormfitt += ", ffid_index";
selectFormfitt += ", fillid";
selectFormfitt += ", file";
selectFormfitt += " FROM";
selectFormfitt += " formfitt";
selectFormfitt += " WHERE";
selectFormfitt += " processid = '{0}'";

let insertFormfitt_previrew = "";
insertFormfitt_previrew += "INSERT INTO formfitt ";
insertFormfitt_previrew += "("
insertFormfitt_previrew += "userid";
insertFormfitt_previrew += " ,processid";
insertFormfitt_previrew += " ,created_at";
insertFormfitt_previrew += " ,ffid";
insertFormfitt_previrew += " ,request";
insertFormfitt_previrew += " ,response";
insertFormfitt_previrew += " ,fillid";
insertFormfitt_previrew += " ,file";
insertFormfitt_previrew += " ,request1";
insertFormfitt_previrew += " ,response1";
insertFormfitt_previrew += ") ";
insertFormfitt_previrew += "VALUES";
insertFormfitt_previrew += "(";
insertFormfitt_previrew += "'{0}'"; //userid
insertFormfitt_previrew += ", '{1}'";  //processid
insertFormfitt_previrew += ", '{2}'"; //created_at
insertFormfitt_previrew += ", '{3}'"; //ffid
insertFormfitt_previrew += ", '{4}'"; //request
insertFormfitt_previrew += ", '{5}'"; //response
insertFormfitt_previrew += ", '{6}'"; //fillid
insertFormfitt_previrew += ", '{7}'"; //file
insertFormfitt_previrew += ", ''"; //request1
insertFormfitt_previrew += ", ''"; //response1
insertFormfitt_previrew += ") ";
insertFormfitt_previrew += "ON DUPLICATE KEY UPDATE ";
insertFormfitt_previrew += " created_at = '{2}'";
insertFormfitt_previrew += " ,ffid = '{3}'";
insertFormfitt_previrew += " ,request = '{4}'";
insertFormfitt_previrew += " ,response = '{5}'";
insertFormfitt_previrew += " ,fillid = '{6}'";
insertFormfitt_previrew += " ,file = '{7}'";
insertFormfitt_previrew += " ,file_audit = '-1'";
insertFormfitt_previrew += " ,ffid_index = {8}";
insertFormfitt_previrew += " ,request1 = '-1'";
insertFormfitt_previrew += " ,response1 = '-1'";
/*
let updateFormfitt = "";
updateFormfitt += "UPDATE formfitt SET ";
updateFormfitt += "file_audit = '{0}'";
updateFormfitt += ", request1 = '{1}'";
updateFormfitt += ", response1 = '{2}'";
updateFormfitt += ", fillid_audit = '{3}'";
updateFormfitt += ", ffid = '{5}'";
updateFormfitt += ", ffid_index = {6}";
updateFormfitt += " WHERE ";
updateFormfitt += "processid = '{4}'";
*/
module.exports = {
    method: 'POST',
    path: '/documents',
    handler: async (request, h) => {

        if(!request.mysql || !request.mysql.pool) {
            console.error("mysql not added");
            throw Boom.notImplemented("mysql not added");
        }

        const pool = request.mysql.pool;
        let token = request.payload.token;  //processid
        let pType = request.payload.type;
        let returnImages = request.payload.images;
        let returnPdf = request.payload.pdf;

        let formattedResponse = {
            statusCode: 200
        };


        //get data for document
        let selectPolicyDatasb = new StringBuilder();
        selectPolicyDatasb.appendFormat(selectPolicyDataString, token);
        let selectPolicy_sql = selectPolicyDatasb.toString();
        let selectPolicyData = await mySQLQuery(pool, selectPolicy_sql);

        if (selectPolicyData.length == 0){
            console.error('unknown token');
            throw Boom.unauthorized('unknown token');    
        } 

        //get current date
        let now_il_d = moment().tz("Asia/Jerusalem").format('DD/MM/YYYY');
        let now_il = moment().tz("Asia/Jerusalem").format('YYYY-MM-DD HH:mm:ss');
        
        //check for existing formfitt record
        let selectFormfittsb = new StringBuilder();
        selectFormfittsb.appendFormat(selectFormfitt, token);
        let selectFormfitt_sql = selectFormfittsb.toString();
        let selectFormfittData = await mySQLQuery(pool, selectFormfitt_sql);

        //if no record found and not a preview request means
        //trying to create final with no preview first (not good)
        if (selectFormfittData.length == 0 && pType != 'preview'){
            console.error("no preview found... cannot produce final pdf");
            throw Boom.badRequest("no preview found... cannot produce final pdf");
        }

        let ff_userid = token + "_" + "0";
        let ff_userid_index = 0;
        //if a preview was previously produced, need to manipulate
        //formfitt user id to avoid "file already exists for this user" message
        if (selectFormfittData.length != 0){
            ff_userid_index = selectFormfittData[0].ffid_index + 1
            ff_userid = token + "_" + ff_userid_index;
        }

        if (pType == 'preview'){
            //create start/birth date format for document
            let sDate = moment(selectPolicyData[0].start_date).tz("Asia/Jerusalem").format('DD/MM/YYYY');
            let bDate = moment(selectPolicyData[0].birth_date).tz("Asia/Jerusalem").format('DD/MM/YYYY');

            //let pData = JSON.parse(selectPolicyData[0].process_data);
            let pData = selectPolicyData[0].process_data;
            let vatika = 0
            if (pData.vatika) vatika = pData.vatika; 
            let menahalim = 0
            if (pData.menahalim) menahalim = pData.menahalim; 
                    
            //payload for formfitt request (if preview) 
            let ffmethod = "create";
            let payload = ffPayload_p({
                id: ff_userid,
                employeeid: selectPolicyData[0].userid,
                firstName: selectPolicyData[0].first_name,
                familyName: selectPolicyData[0].family_name,
                gender: selectPolicyData[0].gender,
                familyStatus: selectPolicyData[0].family_status,
                city: selectPolicyData[0].city,
                street: selectPolicyData[0].street,
                house: selectPolicyData[0].house,
                cell: selectPolicyData[0].cell,
                email: selectPolicyData[0].email,
                employerName: selectPolicyData[0].employerName,
                employerCity: selectPolicyData[0].employerCity,
                employerHouse: selectPolicyData[0].employerHouse,
                employerStreet: selectPolicyData[0].employerStreet,
                employerid: selectPolicyData[0].employerid,
                employerPhone: selectPolicyData[0].employerPhone,
                startDate: sDate,
                birthDate: bDate,
                fundName: selectPolicyData[0].keren_name,
                depositPercent: selectPolicyData[0].dmei_ninul_hafkada, 
                accumulatePercent: selectPolicyData[0].dmei_nihul_tzvira, 
                today: now_il_d,
                formid: selectPolicyData[0].formid,
                sign_1: pData.sign_1,
                vatika: vatika,
                menahalim: menahalim,
                connectionMethod: 1,
                x_erase: "X"
            });

            let ff = await ffPost(ffmethod, payload);
            ffResponse = await parseFFResponse(ff);
    
            if(ffResponse.form.info[0].status[0] == "Error"){
                var msg = "formfitt error: ";
                if (ffResponse.form.info[0].message[0]){
                    msg += ffResponse.form.info[0].message[0];
                }
                console.error(msg);
                throw Boom.badRequest(msg);
            }
    
            let updateFormfitt_sql = "";
            let updateFormfittsb = new StringBuilder();
            //set data for document
            updateFormfittsb.appendFormat(insertFormfitt_previrew
                , selectPolicyData[0].userid
                , token
                , now_il
                , ff_userid
                , payload
                , ff
                , ffResponse.form.info[0].fillid[0]
                , ffResponse.form.info[0].pdf[0]
                , ff_userid_index
            );

            //update formfitt table 
            updateFormfitt_sql = updateFormfittsb.toString();
            let updateFF = await mySQLQuery(pool, updateFormfitt_sql);
            
            //
            //download the newly created file from formfitt
            //create images and save to dropbox
            //
            let pdf = await ffGetFile(pType, {
                uri: ffResponse.form.info[0].pdf[0],
                pid: token,
                images: returnImages,
                pdf: returnPdf,
                firstName: selectPolicyData[0].first_name,
                familyName: selectPolicyData[0].family_name,
                clientid: selectPolicyData[0].clientid,
                employerid: selectPolicyData[0].employerid,
                userid: selectPolicyData[0].userid
            });
            if (pdf.statusCode != 200){
                Boom.serverUnavailable("document server error", pdf.error);
            }
            
            if (returnPdf == 1){
                formattedResponse.file = pdf.file;
            }
            if (returnImages == 1){
                formattedResponse.images = pdf.images;
            }
        

        }
        //payload for audit page (if final) 
        if (pType == 'final'){
            //fill audit fields
            let contactedBy = "_";
            if (selectPolicyData[0].contacted_by){
                contactedBy = selectPolicyData[0].contacted_by;
            }

            let createdAt = "_";
            let createdAt1 = "_";
            if (selectPolicyData[0].documentCreated_at){
                createdAt = moment(selectPolicyData[0].documentCreated_at).format('DD/MM/YYYY');
                createdAt1 = moment(selectPolicyData[0].documentCreated_at).format('DD/MM/YYYY hh:mm a');
            }

            let createdBy = "_";
            if (selectPolicyData[0].created_by){
                createdBy = selectPolicyData[0].created_by;
            }

            let fileId = "_";
            if (selectFormfittData[0].ffid){
                fileId = selectFormfittData[0].ffid;
            }
            let fillId = "_";
            if (selectFormfittData[0].fillid){
                fillId = selectFormfittData[0].fillid;
            }
            fileId = fileId + "/" + fillId;

            let CreatedRemoteAddress = "_";
            if (selectPolicyData[0].documentCreated_remoteAddress){
                CreatedRemoteAddress = selectPolicyData[0].documentCreated_remoteAddress;
            }

            let viewedAt = "_";
            if (selectPolicyData[0].documentViewed_at){
                viewedAt = moment(selectPolicyData[0].documentViewed_at).format('DD/MM/YYYY hh:mm a');
            }

            let viewedRemoteAddress = "_";
            if (selectPolicyData[0].documentViewed_remoteAddress){
                viewedRemoteAddress = selectPolicyData[0].documentViewed_remoteAddress;
            }

            let signedAt = "_";
            if (selectPolicyData[0].documentSigned_at){
                signedAt = moment(selectPolicyData[0].documentSigned_at).format('DD/MM/YYYY hh:mm a');
            }

            let signedRemoteAddress = "_";
            if (selectPolicyData[0].doumentSigned_remoteAddress){
                signedRemoteAddress = selectPolicyData[0].doumentSigned_remoteAddress;
            }

            let lockedAt = "_";
            if (selectPolicyData[0].documentLocked_at){
                lockedAt = moment(selectPolicyData[0].documentLocked_at).format('DD/MM/YYYY hh:mm a');
            }

            let lockedRemoteAddress = "_";
            if (selectPolicyData[0].documentLocked_remoteAddress){
                lockedRemoteAddress = selectPolicyData[0].doumentLocked_remoteAddress;
            }
    
            let payload_audit_page = {
                audit1: selectPolicyData[0].name,
                audit2: "Contacted By: " + contactedBy,
                audit3: "Created: " + createdAt,
                audit4: "By: " + createdBy,
                audit5: "Status: APPROVED",
                audit6: "File ID: " + fileId,
                audit7: "Audit Trail:",
                audit8: "* Document created by " + createdBy,
                audit9: createdAt1 + ", IP Address: " + CreatedRemoteAddress,
                audit10: "* Document viewed by: " + selectPolicyData[0].name,
                audit11: viewedAt + ", IP Address: " + viewedRemoteAddress,
                audit12: "* Document signed by: " + selectPolicyData[0].name,
                audit13: signedAt + ", IP Address: " + signedRemoteAddress,
                audit14: "* Document locked by: " + selectPolicyData[0].name,
                audit15: lockedAt + ", IP Address: " + lockedRemoteAddress
            };

        }

        
        //
        //download the newly created file from formfitt
        //create images and save to dropbox
        //
        let pdf = await ffGetFile(pType, {
            uri: ffResponse.form.info[0].pdf[0],
            pid: token,
            images: returnImages,
            pdf: returnPdf,
            firstName: selectPolicyData[0].first_name,
            familyName: selectPolicyData[0].family_name,
            clientid: selectPolicyData[0].clientid,
            employerid: selectPolicyData[0].employerid,
            userid: selectPolicyData[0].userid
        });
        if (pdf.statusCode != 200){
            Boom.serverUnavailable("document server error", pdf.error);
        }
        
        if (returnPdf == 1){
            formattedResponse.file = pdf.file;
        }
        if (returnImages == 1){
            formattedResponse.images = pdf.images;
        }

        const response = h.response(formattedResponse);
        return response;
    },
    options: {
        validate: {
            payload: {
                token: Joi.required(),
                type: Joi.string().valid('final', 'preview').required(),
                images: Joi.number().valid([0,1]).required(),
                pdf: Joi.number().valid([0,1]).required()
            }
        }
    }
};

function parseFFResponse(resp) {
    return new Promise(function(resolve, reject) {
        try {
            parseXML(resp, function (err, result) {
                if (err) {
                    //return reject(err);
                    reject(err);
                } else {
                    //return resolve(rows);
                    resolve(result);
                }
            });
        } catch (err) {
            //return reject(err);
            reject(err);
        }
    });
}

/*
<?xml version="1.0" encoding="UTF8"?>
<form>
<info>
<key>74ad85d8e367a7b0d522fce7e2746d9b</key>
<formid>empty</formid>
<id>fa7f64c4-2d87-40e6-9bed-45b66c9a_</id>
<externalid>1111</externalid>
</info>
<fields>
    <field name="ident1">IDENT VAL 1 UPDATE</field>
    <field name="ident2">IDENT VAL 2</field>
    <field name="cbox1">1</field>
    <field name="cbox2">0</field>
</fields>
</form>

<xml version="1.0" encoding="UTF-8">
    <form>
        <info>
            <status>Success</status>
            <formid>empty</formid>
            <id>304546351</id>
            <link>https://formfitt.com/Forms/Fill/Form/5b1548d7a46ea/5b1550b42f119</link>
            <pdf>https://formfitt.com/Forms/API/PDF/5b1548d7a46ea/5b1550b42f119</pdf>
            <fillid>5b1550b42f119</fillid>
            <externalid>304546347</externalid>
        </info>
    </form>

    
*/