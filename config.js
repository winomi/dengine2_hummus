const env = 1; //dev
//const env = 2; //test
//const env = 3; //prod

//dev
let dropbox = 'XoX1zAGxdOAAAAAAAAAALjvFqPFY_KMAoHnps01IMjp9DyurxI0ZM43YQFLTV5Bn';//test
let rollbar = "d62e7259b6104b4a8b3c7640029a4047";
let mySql = {
    connectionLimit : 10,
    host: 'localhost',
    port: 3306,
    user: 'metafel',
    password: '1111',
    database: 'metafel'
} 

//test
if (env == 2){
    mySql = {
        connectionLimit : 10,
        host: 'ktree-mydb.mysql.database.azure.com',
        port: 3306,
        user: 'metafel@ktree-mydb',
        password: '1111!@1',
        database: 'metafel-test',
        ssl: true
    }
}

//prod
if (env == 3){
    dropbox = 'XoX1zAGxdOAAAAAAAAAANxVmxm_QSQ7gf0u11M6Bb9steMaU-aIEcL0FYozjZxdc';
    rollbar = '8180ab03fbfa4783bfe119ca934ac120';
    mySql = {
        connectionLimit : 10,
        host: 'ktree-mydb.mysql.database.azure.com',
        port: 3306,
        user: 'metafel@ktree-mydb',
        password: '1111!@1',
        database: 'metafel',
        ssl: true
    }
}

module.exports = {
    authKey: 'UxkMF9PM1mMzlz0sZkREEdfiT9FGbRer',
    env: env,
    rollbar: rollbar,
    mySql: mySql,
    dropbox: dropbox
};

